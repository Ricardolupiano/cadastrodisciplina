package controller;

import java.util.ArrayList;

import model.Aluno;
import model.Disciplina;

public class ControleDisciplina{

//Atributos

private ArrayList<Disciplina> listaDisciplinas;

//Construtor
	public ControleDisciplina(){
		listaDisciplinas = new ArrayList<Disciplina>();
	}	

//Metodos
	public void adicionarDisciplina(Disciplina umaDisciplina){
		listaDisciplinas.add(umaDisciplina);
	}
	
	public void removerDisciplina(Disciplina umaDisciplina){
		listaDisciplinas.remove(umaDisciplina);
	}

	public void exibirDisciplinas(){
		for(Disciplina umaDisciplina: listaDisciplinas){
			System.out.println("Nome: "+ umaDisciplina.getNomeDisciplina());
            System.out.println("Codigo: " + umaDisciplina.getCodigo());
		}
	}
	
	public Disciplina pesquisarNome(String umNome){
		for(Disciplina umaDisciplina: listaDisciplinas){
			if(umaDisciplina.getNomeDisciplina().equalsIgnoreCase(umNome)){
				return umaDisciplina;
			}	
		}
		return null;
	}
	
	public Disciplina pesquisarCodigo(String umNome){
		for(Disciplina umaDisciplina: listaDisciplinas){
			if(umaDisciplina.getCodigo().equalsIgnoreCase(umNome)){
				return umaDisciplina;
			}
		}
		return null;
	}
    public void matriculaAluno(Aluno umNome, Disciplina umaDisciplina){
        umaDisciplina.addNomeAluno(umNome);
    }
    public void removerMatriculaAluno (Aluno umNome, Disciplina umaDisciplina){
        umaDisciplina.removeNomeAluno(umNome);
    }
    public void mostrarAlunosDisciplina (Disciplina umaDisciplina){
        umaDisciplina.exibirAlunos(umaDisciplina);
    }
    public ArrayList<Disciplina> getListaDisciplinas(){
		return listaDisciplinas; 
	}
}

