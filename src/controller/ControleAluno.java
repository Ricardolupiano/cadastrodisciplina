package controller;

import java.util.ArrayList;

import model.Aluno;

public class ControleAluno{
//Atributos
	private ArrayList<Aluno> listaAlunos;
	
//construtor
	public ControleAluno(){
	listaAlunos = new ArrayList<Aluno>();
	}
//metodos

	public void adicionar(Aluno umAluno){
		listaAlunos.add(umAluno);
	}
	public void remover(Aluno umAluno){
		listaAlunos.remove(umAluno);
    }
	public Aluno pesquisarNome(String umNome){
		for(Aluno umAluno: listaAlunos){
			if(umAluno.getNome().equalsIgnoreCase(umNome)){
				return umAluno;
			}
		}
        return null;
	}
	public void exibirAlunos(){
		for (Aluno umAluno:listaAlunos){
			System.out.println("Nome: " + umAluno.getNome());
            System.out.println("Matricula: " + umAluno.getMatricula());
		}
	}
	public ArrayList<Aluno> getListaAlunos(){
		return listaAlunos; 
	}
	
	
}
