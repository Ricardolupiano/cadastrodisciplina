package view;

import java.io.*;

import model.Aluno;
import model.Disciplina;

import controller.ControleAluno;
import controller.ControleDisciplina;

public class CadastroAlunoDisciplina{
	

    public static void main(String[] args) throws IOException{

        // burocracia
        InputStream entradaSistema = System.in;
        InputStreamReader leitor = new InputStreamReader(entradaSistema);
        BufferedReader leitorEntrada = new BufferedReader(leitor);
        String entradaTeclado;

        //declarando variaveis
        String umNome;
        String umCodigo;
        String umaMatricula;
        String umaMateria;
        
        char menu = 'a';
        //instanciando objetos

        ControleAluno umControleAluno = new ControleAluno();
        ControleDisciplina umControleDisciplina = new ControleDisciplina();
        Aluno umAluno = new Aluno();
        Disciplina umaDisciplina = new Disciplina();

        // interagindo

        while (menu != 'b'){

            System.out.println("=======MENU=======");
            System.out.println("1- Adicionar Disciplina");
            System.out.println("2- Remover Disciplina");
            System.out.println("3- Listar Disciplinas");
            System.out.println("4- Adicionar aluno");
            System.out.println("5- Remover aluno");
            System.out.println("6- Associar Aluno-Disciplina");
            System.out.println("7- Desassociar Aluno-Disciplica");
            System.out.println("8- Listar alunos Geral");
            System.out.println("9- Listar alunos de uma Disciplina especifica");
            System.out.println("s- Sair");
            System.out.println("Digite a opção desejada: ");
	
            entradaTeclado = leitorEntrada.readLine();
            menu = entradaTeclado.charAt(0);
	
            if(menu == '1'){
                System.out.println("Insira o nome da disciplina: ");
                entradaTeclado = leitorEntrada.readLine();
                umNome = entradaTeclado;
                System.out.println("Insira o codigo da disciplina:");
                entradaTeclado = leitorEntrada.readLine();
                umCodigo = entradaTeclado;
                umaDisciplina = new Disciplina(umNome, umCodigo);
                umControleDisciplina.adicionarDisciplina(umaDisciplina);
            }

            else if(menu == '2'){
                System.out.println("Insira o nome da disciplina a ser removida: ");
                entradaTeclado = leitorEntrada.readLine();
                umNome = entradaTeclado;
                umaDisciplina = umControleDisciplina.pesquisarNome(umNome);
                umControleDisciplina.removerDisciplina(umaDisciplina);
            }
            else if(menu== '3'){
                umControleDisciplina.exibirDisciplinas();
            }
            else if(menu== '4'){
                System.out.println("Digite o nome do aluno a ser adicionado: ");
                entradaTeclado = leitorEntrada.readLine();
                umNome = entradaTeclado;
                System.out.println("Digite a matricula do aluno: ");
                entradaTeclado = leitorEntrada.readLine();
                umaMatricula = entradaTeclado;
                umAluno = new Aluno (umNome, umaMatricula);
                umControleAluno.adicionar(umAluno);
            }
            else if(menu == '5'){
                System.out.println("Digite o nome do aluno a ser removido: ");
                entradaTeclado = leitorEntrada.readLine();
                umNome = entradaTeclado;
                umAluno = umControleAluno.pesquisarNome(umNome);
                umControleAluno.remover(umAluno);
            }
            else if(menu=='6'){
                System.out.println("Alunos já matriculados: ");
                umControleAluno.exibirAlunos();
                System.out.println("Disciplinas já cadastradas: ");
                umControleDisciplina.exibirDisciplinas();
                System.out.println("Digite o nome do aluno para fazer a associação: ");
                entradaTeclado = leitorEntrada.readLine();
                umNome = entradaTeclado;
                umAluno = umControleAluno.pesquisarNome(umNome);
                System.out.println("Digite a disciplina para fazer a associação: ");
                entradaTeclado = leitorEntrada.readLine();
                umaMateria = entradaTeclado;
                umaDisciplina = umControleDisciplina.pesquisarNome(umaMateria);
                umControleDisciplina.matriculaAluno(umAluno,umaDisciplina);
            }
            else if(menu=='7'){
                System.out.println("Alunos já matriculados: ");
                umControleAluno.exibirAlunos();
                System.out.println("Disciplinas já cadastradas: ");
                umControleDisciplina.exibirDisciplinas();
                System.out.println("Digite o nome do aluno para desfazer a associação: ");
                entradaTeclado = leitorEntrada.readLine();
                umNome = entradaTeclado;
                umAluno = umControleAluno.pesquisarNome(umNome);
                System.out.println("Digite a disciplina para desfazer a associação: ");
                entradaTeclado = leitorEntrada.readLine();
                umaMateria = entradaTeclado;
                umaDisciplina = umControleDisciplina.pesquisarNome(umaMateria);
                umControleDisciplina.removerMatriculaAluno(umAluno,umaDisciplina);
            }
            else if (menu=='8'){
                umControleAluno.exibirAlunos();
            }
            else if (menu=='9'){
                System.out.println("Disciplinas já cadastradas: ");
                umControleDisciplina.exibirDisciplinas();
                System.out.println("Digite a disciplina desejada: ");
                entradaTeclado = leitorEntrada.readLine();
                umaMateria = entradaTeclado;
                umaDisciplina = umControleDisciplina.pesquisarNome(umaMateria);
                umControleDisciplina.mostrarAlunosDisciplina(umaDisciplina);
            }
            else if (menu=='s'){
                menu = 'b';
            }
            else{
                System.out.println("Opção inválida, tente novamente!");
            }
        }
    }
}
