package model;

import java.util.ArrayList;

public class Disciplina{
	
//atributos
	private String nomeDisciplina;
	private String codigo;
	private ArrayList<Aluno> listaAlunos;

//construtor
	public Disciplina(){
	}
	public Disciplina(String umNome, String codigoDisciplina){
		nomeDisciplina = umNome;
		codigo = codigoDisciplina;
        listaAlunos = new ArrayList<Aluno>();
	}
//metodos

	public void setNomeDisciplina(String umNome){
		nomeDisciplina = umNome;
	}
	public String getNomeDisciplina(){
		return nomeDisciplina;
	}

	public void setCodigo(String codigoDisciplina){
		codigo = codigoDisciplina;
	}

	public String getCodigo(){
		return codigo;
	}

	public void addNomeAluno(Aluno umNome){
		listaAlunos.add(umNome);
	}
    public void removeNomeAluno(Aluno umNome){
        listaAlunos.remove(umNome);
    }
    public void exibirAlunos(Disciplina umaDisciplina) {
		for (Aluno umAluno : umaDisciplina.listaAlunos) {
			System.out.println("Nome: " + umAluno.getNome() + " Matricula: " + umAluno.getMatricula());
		}
	}
    
}	
