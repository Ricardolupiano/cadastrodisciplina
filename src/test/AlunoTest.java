package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import model.Aluno;

public class AlunoTest {
	Aluno umAluno;
	@Before
	public void setUp() throws Exception{
	}

	@Test
	public void testNomeAluno() {
		umAluno = new Aluno();
		umAluno.setNome("Joao");
		assertEquals("Joao",umAluno.getNome());
	}
	@Test
	public void testMatriculaAluno() {
		umAluno = new Aluno();
		umAluno.setMatricula("13001");
		assertEquals("13001",umAluno.getMatricula());
	}

}
