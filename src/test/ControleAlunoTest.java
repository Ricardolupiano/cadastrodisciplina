package test;

import static org.junit.Assert.*;

import model.Aluno;

import org.junit.Before;
import org.junit.Test;

import controller.ControleAluno;

public class ControleAlunoTest {
	ControleAluno umControleAluno;
	Aluno umAluno = new Aluno("Joao","13000");
	
	@Before
	public void setUp() throws Exception{
	}
	@Test
	public void testAdicionar() {
		umControleAluno = new ControleAluno();
		umControleAluno.adicionar(umAluno);
		Aluno alunoPesquisado = umControleAluno.pesquisarNome("Joao");
		assertEquals("Joao", alunoPesquisado.getNome());
	}	
	@Test
	public void testRemover() {
		umControleAluno = new ControleAluno();
	
		umControleAluno.adicionar(umAluno);
		umControleAluno.adicionar(umAluno);
		umControleAluno.adicionar(umAluno);
		assertEquals(3,umControleAluno.getListaAlunos().size());
		
		umControleAluno.remover(umAluno);
		assertEquals(2,umControleAluno.getListaAlunos().size());
		
	}

}
