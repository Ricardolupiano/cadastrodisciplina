package test;

import static org.junit.Assert.*;
import model.Disciplina;

import org.junit.Before;
import org.junit.Test;

public class DisciplinaTest {
	Disciplina umaDisciplina;
	
	@Before
	public void setUp() throws Exception{
	}
	@Test
	public void testNome() {
		 umaDisciplina = new Disciplina();
		 umaDisciplina.setNomeDisciplina("Calculo");
		assertEquals ("Calculo",umaDisciplina.getNomeDisciplina());
	}
	@Test
	public void testCodigo(){
		umaDisciplina = new Disciplina();
		umaDisciplina.setCodigo("777");
		assertEquals ("777",umaDisciplina.getCodigo());
	}

}
