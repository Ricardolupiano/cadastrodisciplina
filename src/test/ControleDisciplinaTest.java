package test;

import static org.junit.Assert.*;

import model.Aluno;
import model.Disciplina;

import org.junit.Before;
import org.junit.Test;

import controller.ControleAluno;
import controller.ControleDisciplina;

public class ControleDisciplinaTest {
	ControleDisciplina umControleDisciplina;
	Disciplina umaDisciplina = new Disciplina("matematica","000");
	
	@Before
	public void setUp() throws Exception{
	}
	
	@Test
	public void testAdicionar() {
		umControleDisciplina = new ControleDisciplina();
		umControleDisciplina.adicionarDisciplina(umaDisciplina);
		Disciplina disciplinaPesquisada= umControleDisciplina.pesquisarNome("matematica");
		assertEquals("matematica", disciplinaPesquisada.getNomeDisciplina());
	}
	@Test
	public void testRemover() {
		umControleDisciplina = new ControleDisciplina();
	
		umControleDisciplina.adicionarDisciplina(umaDisciplina);
		umControleDisciplina.adicionarDisciplina(umaDisciplina);
		umControleDisciplina.adicionarDisciplina(umaDisciplina);
		assertEquals(3,umControleDisciplina.getListaDisciplinas().size());
		
		umControleDisciplina.removerDisciplina(umaDisciplina);
		assertEquals(2,umControleDisciplina.getListaDisciplinas().size());
		
	}

}
